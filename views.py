#coding:utf-8

from flask.views import View,MethodView
from flask import session,render_template,g,request,flash,Markup,\
     escape,redirect

import settings
from settings import app

import pdb




@app.context_processor
def show_comments():
    cur = g.db.execute('select co_text from comments')
    comments = cur.fetchall()
    return dict(comments=comments)
    

@app.context_processor
def headers():
    headers = request.headers.get('User-Agent')
    return dict(headers=headers)



class PostAPI(MethodView):

    def __init__(self):
        pass

    def get(self,post_id):
        #pdb.set_trace()
        cur_entries = g.db.execute("select title,text,comment_on,created from \
                    entries where id=%d"% post_id)
        post = [dict(title=row[0],text=row[1],comment_on=bool(row[2]),created=row[3]) for \
                 row in cur_entries.fetchall()]
        cur_comments = g.db.execute('select co_title,co_text,author,co_created from \
                    comments where post_id=%d order by co_id desc'% post_id)
        comments = [dict(title=row[0],text=row[1],author=row[2],created=row[3]) for \
                    row in cur_comments.fetchall()]
        post[0]['comments']= comments
        post[0]['post_id'] = post_id
        return render_template('posts.html',post=post)

    def post(self):
        if 'logged_in' not in session:
            abort(401)
        elif request.form['title'] == '' or request.form['text'] == '':
            flash (u"The title and text couldn't be empty ")
        else:
            """
            I didn't believe the dada from client is current always,
            so, there need a handler to ensure the data 'comment_on' must be
            the boolean True/False
            """
            comment_on = request.form['comment_on']
            if comment_on == u'False' or comment_on == u'1' or comment_on == False:
                comment_on = False
            else :
                comment_on = True
            g.db.execute( 'insert into entries (title,text,comment_on) values (?,?,?)',
                        (Markup(request.form['title']).striptags(),Markup(request.form['text']),comment_on) )
            g.db.commit()
            flash('New entry was successfully posted')
        return redirect('/')

post_api = PostAPI.as_view('post_api')
app.add_url_rule('/posts/<int:post_id>/',
                 view_func=post_api,
                 methods=['GET',])

app.add_url_rule('/posts/',
                 view_func=post_api,
                 methods=['POST',])


class BaseView(View):

    def __init__(self):
        pass
    
    def get_template_name(self):
        raise NotImplementedError('There is no template')
    
    def render_template(self,context):
        return render_template(self.get_template_name(),**context)

    def get_objects(self):
        pass

    def dispatch_request(self,now_page):
        context = {'objects':self.get_objects(now_page)}
        return self.render_template(context)

class PageView(BaseView):
    # method = ['GET','POST']
    def get_template_name(self):
        return 'pages.html'
        
    def get_objects(self,now_page):
        pagesize = 6
        pagenum = {}
        maxpage = {}
        pagenum['page'] = now_page
        maxpage['maxpage'] = g.db.execute("select(((select count(*) from entries)+(%d)-1)/(%d)) as 'maxpage'" % \
                                          (pagesize,pagesize)).fetchone()[0] #(x+y-1)/y
        
        cur_entries = g.db.execute("select id,title,text,created from entries order by id desc limit %s,%s"% \
                                   ((now_page-1)*pagesize,pagesize))
        
        posts = [dict(post_id=row[0],title=row[1],text=row[2],created=row[3]) for \
                 row in cur_entries.fetchall()]
        pages = [pagenum,maxpage,posts]
        return pages

page_view = PageView.as_view('page_view')
app.add_url_rule('/',
                 defaults={'now_page':1},
                 view_func=page_view)

app.add_url_rule('/posts/',
                 defaults={'now_page':1},
                 view_func=page_view,
                 methods=['GET'])

app.add_url_rule('/pages',
                 defaults={'now_page':1},
                 view_func=page_view)

app.add_url_rule('/pages/<int:now_page>/',
                 view_func=page_view)

