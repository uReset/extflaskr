#coding:utf-8
"""
    extflaskr
    ~~~~~~
    a microblog forked from flaskr a simple app by flask and sqlite3
"""

import os
import time
import pdb

from flask import Flask,request,session,redirect,url_for,abort,\
     render_template,g,flash,Markup,escape,send_from_directory

from settings import app
import settings
import views




##@app.route('/posts')
##@app.route('/')
##def show_entries():
##    cur = g.db.execute('select id,title,text,created from entries order by id desc')
##    entries = [dict(post_id=row[0],title=row[1],text=row[2],created=row[3]) for \
##               row in cur.fetchall()]
##    return render_template('show_entries.html',entries=entries)


##@app.route('/posts/<int:post_id>/',methods=['GET'])
##def post_by_id(post_id):
##    #pdb.set_trace()
##    if request.method == 'GET':
##        cur_entries = g.db.execute("select title,text,comment_on,created from entries where id=%s" % post_id)
##        post = [ dict(title=row[0],text=row[1],comment_on=bool(row[2]),created=row[3]) for \
##                 row in cur_entries.fetchall() ]     #The 'comment_on' from sql maybe not a boolean True/False ,it maybe a string or number beacuse of the sqlite3,so I do a bool(),that the date will be a boolean 
##        cur_comments = g.db.execute('select co_title,co_text,author,co_created from comments where post_id=%s order by co_id desc' % post_id)
##        comments = [dict(title=row[0],text=row[1],author=row[2],created=row[3]) for \
##                    row in cur_comments.fetchall()]
##        post[0]['comments']= comments
##        post[0]['post_id'] = post_id
##        return render_template('posts.html',post=post)
##    else:
##        return 'Invalid post id !'

##@app.route('/')
##@app.route('/pages/',methods=['GET'])
##@app.route('/pages/<int:page>',methods=['GET'])
##def pages_views(page=1):
##    pagesize = 6
##    num_page = {}
##    maxpage = {}
##    num_page['page'] = page
##    maxpage['maxpage'] =g.db.execute("select(((select count(*) from entries)+ (%d) -1)/ (%d)) as 'maxpage'" %(pagesize,pagesize)).fetchone()[0]
##    cur_entries = g.db.execute("select id,title,text,created from entries order by id desc limit %s,%s"%((page-1)*pagesize,pagesize))
##    posts = [dict(post_id=row[0],title=row[1],text=row[2],created=row[3]) for \
##             row in cur_entries.fetchall()]
##    pages = [num_page,maxpage,posts]
##    return render_template('pages.html',pages=pages)


##@app.route('/add',methods=['GET','POST'])
##def add_entry():
##    if 'logged_in' not in session:
##        abort(401)
##    elif request.form['title'] == '' or request.form['text'] == '':
##        flash (u"The title and text couldn't be empty ")
##    else:
##        """
##        I didn't believe the dada from client is current always,
##        so, there need a handler to ensure the data 'comment_on' must be the boolean True/False
##        """
##        comment_on = request.form['comment_on']
##        if comment_on == u'False' or comment_on == u'1' or comment_on == False:
##            comment_on = False
##        else :
##            comment_on = True
##        g.db.execute( 'insert into entries (title,text,comment_on) values (?,?,?)',
##                    (Markup(request.form['title']).striptags(),Markup(request.form['text']),comment_on) )
##        g.db.commit()
##        flash('New entry was successfully posted')
##    return redirect('/')

    
@app.route('/posts/<int:post_id>/add_comments',methods=['POST'])
def add_comments(post_id):
    if int(request.form['comment_post_id']) != post_id:
        flash(u"The comment was illegal")
    elif request.form['text'] =='' or request.form['author'] == '':
        flash(u"The nickname and text cound't be empty")
    else:
        g.db.execute('insert into comments (post_id,co_text,author,ip_addr) values (?,?,?,?)',\
                     (post_id,request.form['text'],request.form['author'][:10],request.remote_addr))
        g.db.commit()
        flash('Add Comment Succeed!')
    return redirect('/posts/%s'%(post_id))


@app.route('/login',methods=['GET','POST'])
def login():
    error = None
    if request.method == 'POST':
        userinfo = [dict(username=row[2],password=row[3]) for row in g.db.execute("select * from users")]
        if request.form['username'] != userinfo[0]['username']:
            error = 'Invalid username!'
        elif request.form['password'] != userinfo[0]['password']:
            error = 'Invalid password!'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect('/')
    return render_template('login.html',error=error)
    

@app.route('/logout')
def logout():
    if 'logged_in' in session:
        session.pop('logged_in',None)
        flash('You were logged out')
        return redirect('/')
    else:
        return 'Need not to logout because you are not login'


@app.errorhandler(404)
def not_found(error):
    flash(u'#Error:404 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This page is not found.')
    return render_template('40x.html'),404

@app.errorhandler(401)
def un_authorized(error):
    flash(u'You are not authorized')
    return render_template('40x.html'),401


@app.route('/posts/<post_title>',methods=['GET'])
def post_by_title(post_title):
    post = [dict(title=row[0],text=row[1])\
            for row in g.db.execute('select title,text from entries where title="%s"'% post_title)]
    return render_template('posts.html',post=post)

@app.route('/favicon.ico',methods=['GET'])
def favicon():
    return send_from_directory(os.path.join(os.getcwd(),'static'),'favicon.ico')




if __name__ == '__main__':
    app.run(host='0.0.0.0',port=8000,debug=True)
