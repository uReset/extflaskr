drop table if exists entries;
create table entries(
    id integer primary key autoincrement,
    title text not null,
    text text not null,
    comment_on int not null default '1',
    created  datetime default ( datetime( 'now', 'localtime' )  ),
    modified datetime default ( datetime( 'now', 'localtime' )  )
);
create table users(
    id integer primary key autoincrement,
    user_id integer not null,
    username text not null,
    password text not null
);

insert into users (user_id,username,password) values ('000','admin','default');

CREATE TABLE comments (
    co_id integer primary key autoincrement not null unique,
    post_id integer not null references entries ( id ),
    co_title text,
    co_text text,
    author text,
    co_created datetime default ( datetime( 'now', 'localtime' )  ),
    ip_addr text
);