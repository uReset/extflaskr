#conding:utf-8
"""
    The settings of flaskr
"""

import os
import sqlite3
from contextlib import closing

from flask import Flask,g


HERE = os.path.abspath('.')

#Configuration
#The database path
DATABASE = os.path.join(HERE,'extflaskr.db')
#debug mode
DEBUG = False
#the secret_key must be as random as possible,you can use
# >>import os
# >>os.urandom(32)
#this can be used to generate a random key
SECRET_KEY = '\x97\xbd\xc0\x0e\xe8l\x83\xa8\x8f\x18U\x12\x8b\xba\x1f\x86\xdf\xe1\x98\xbb\xd8@|(L@\xaf\x92@\xb7\xf0'
#input your sitename
SITENAME = 'iHex'


#creat the application
app = Flask(__name__)
app.config.from_object(__name__)


def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('extflaskr.sql') as f:
            db.cursor().executescript(f.read())
        db.commit


@app.before_request
def before_request():
    g.db = connect_db()
    
    
@app.teardown_request
def teardown_request(exception):
    g.db.close()


    

if __name__ == '__main__':
    init_db()
